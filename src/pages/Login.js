import {Fragment, useEffect, useState, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
//React Context
import UserContext from '../UserContext'
//routing
import {Redirect, useHistory} from 'react-router-dom'

export default function Login () {
	//The useHistory hook gives you access to the history instance that you may use to navigate/ to access the location
	const history = useHistory();

	//useContext is a react hook used to unwrap our context. It will return the data passed as values by a provider(UserContext.Provider component in App.js)
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password,setPassword] =useState('');
	const [isActive,setIsActive] = useState(false);

	useEffect(() => {
		if (email!==''&& password!=='') {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email,password]);

	let loginUser = e => {
		//fetch has 2 arg
		//1. URL from the server(routes)
		//2. Optional object which contains additional information about our requests such as method, headers, body etc.
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data.accessToken !== undefined) {
				localStorage.setItem('accessToken',data.accessToken);
				setUser({'accessToken':data.accessToken});

				Swal.fire({
					title: 'Yaaaaaaay!!',
					icon: 'success',
					text: 'Thank you for logging in to Zuitt Booking System'
				})

				//get user's details from our token
				fetch('http://localhost:4000/users/details', {
					headers: {
						Authorization:`Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data=> {
					console.log(data)
					if (data.isAdmin === true) {
						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							email:data.setEmail,
							isAdmin:data.isAdmin
						})
						//If Admin, redirect the page to /courses
						//push(path) - pushes a new entry onto the history stack
						history.push('/courses');
					}else{
						//if not Admin, redirect to homepage
						history.push('/');
					}


				})

			}else{
				Swal.fire({
					title: 'Ooops!!',
					icon: 'error',
					text: 'Something went wrong. Check your credentials.'
				})
			}
			setEmail('');
			setPassword('');

		})



		e.preventDefault();
		// setEmail('');
		// setPassword('');

		// Swal.fire(
		// 	{
		// 		title: 'Login successful!',
		// 		icon: 'success',
		// 		text: 'Please wait'
		// 	}
		// )
		// //local storage allows us to save data within our browses as strings\
		// //The setItem() method of the Storage object, or update the key's value if it already exists
		// //setItem is used to store data in the localStorage as a string
		// //setItem('key',value)
		// localStorage.setItem('email', email);
		// setUser({emal: email});
	}
	//Create a conditional statement that will redirect the user to the homepage when a user is logged in.
	return(
		(user.accessToken !== null) ?
			<Redirect to="/"/>
			:
		<Fragment>
			<Form onSubmit ={e=>loginUser(e)}>
				<h1>Login</h1>
				<Form.Group>
					<Form.Label>Email address:</Form.Label>
					<Form.Control 
						type="email"
						placeholder="Enter email"
						value={email}
						onChange= {e=>setEmail(e.target.value)}
						required
					/>
					<Form.Label>Password:</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Enter password"
						value={password}
						onChange= {e=>setPassword(e.target.value)}
						required
					/>
				</Form.Group>
				{isActive ?
					<Button type="submit" variant="success" id="loginBtn">Login</Button>
					:
					<Button type="submit" variant="success" id="loginBtn" disabled>Login</Button>
				}

			</Form>
		</Fragment>
	)


}