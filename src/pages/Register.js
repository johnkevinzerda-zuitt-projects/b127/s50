import {Fragment, useEffect, useState, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Redirect, useHistory} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function Register () {

	//State hooks to store the values of the input fields
	const {user} = useContext(UserContext);
	const [email,setEmail] = useState('');
	const [password1,setPassword1] = useState('');
	const [password2,setPassword2] = useState('');
	const [firstName,setFirstName] = useState('');
	const [lastName,setLastName] = useState('');
	const [mobileNo,setMobileNo] = useState('');
	
	const [isActive,setIsActive] = useState(false);
	const history = useHistory();
	
	//Check if values are successfully binded
	// console.log(email)
	// console.log(password1)
	// console.log(password2)

	useEffect(()=>{
		//Validation to enable submit button when all fields are populated and both passwords matched
		if ((email!=='' && password1  !=='' && password2 !== '' && lastName !== '' && firstName !== '' && mobileNo !== '') && (password1 === password2) && (mobileNo.length >= 11)) {
			setIsActive(true);
		}else{
			setIsActive(false);
		}

	},[email, password1, password2, firstName, lastName, mobileNo])


	let registerUser = e => {
		e.preventDefault();
		//to clear input fields

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res=>res.json())
		.then(data=> {
			if (data === true) {
				Swal.fire(
					{
						title: 'Duplicate email found',
						icon: 'error',
						text: 'Please provide a different email'
					}
		  		)
			}else{

				fetch('http://localhost:4000/users/register',{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email: email,
						password: password2,
						lastName: lastName,
						firstName: firstName,
						mobileNo: mobileNo
					})

				})
				.then(res => res.json())
				.then(data => {
					setEmail('');
					setPassword1('');
					setPassword2('');
					setFirstName('');
					setLastName('');
					setMobileNo('');

					if (data === true) {
					Swal.fire(
						{
							title: 'Registration successful',
							icon: 'success',
							text: 'Welcome to Zuitt!'
						}
				  	)
				  	history.push('/login')
				  } else {
				  	Swal.fire(
						{
							title: 'Something went wrong',
							icon: 'error',
							text: 'Please try again later'
						}
				  	)
				  }
				})
			}
		})		
	}

	//Two way binding
	//THe values in the fields of the form is bound to the getter of the state and the event is bound to the setter. This is called two way binding
	//The data we changed in the view has updated the state
	//The data in the state has updated the view


	/*
	Mini-Activity

	-Redirect the user back to "/" (homepage) route if a user is already logged in.

	*/

	return (
		(user.accessToken !== null) ?
		<Redirect to="/"/>
		:
	<Fragment>
	<h1>Register</h1>
	
	<Form onSubmit={(e)=> registerUser(e)}>
		<Form.Group>
			<Form.Label>Email address:</Form.Label>
			<Form.Control 
			type="email"
			placeholder="Enter email"
			value={email}
			onChange={e=>setEmail(e.target.value)}// the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
			required
			/>
			<Form.Text className="text-muted">
				We'll never share your email with anyone else.
			</Form.Text>
		</Form.Group>
		<Form.Group>
			<Form.Label>Password:</Form.Label>
			<Form.Control
			type="password"
			placeholder="Enter your password"
			value={password1}
			onChange={e=>setPassword1(e.target.value)}
			required
			/>
		</Form.Group>
		<Form.Group>
			<Form.Label>Verify Password:</Form.Label>
			<Form.Control
			type="password"
			placeholder="Verify your password"
			value={password2}
			onChange={e=>setPassword2(e.target.value)}
			required
			/>
		</Form.Group>
		<Form.Group>
			<Form.Label>Last Name:</Form.Label>
			<Form.Control
			type="text"
			placeholder="Last Name"
			value={lastName}
			onChange={e=>setLastName(e.target.value)}
			required
			/>
		</Form.Group>
		<Form.Group>
			<Form.Label>First Name:</Form.Label>
			<Form.Control
			type="text"
			placeholder="First Name"
			value={firstName}
			onChange={e=>setFirstName(e.target.value)}
			required
			/>
		</Form.Group>
		<Form.Group>
			<Form.Label>Mobile No.:</Form.Label>
			<Form.Control
			type="text"
			placeholder="11-digit Mobile No."
			value={mobileNo}
			onChange={e=>setMobileNo(e.target.value)}
			required
			/>
		</Form.Group>
		{isActive ?
		<Button type="submit" variant="primary" id="submitBtn">
			Submit
		</Button>
		:
		<Button type="submit" variant="primary" id="submitBtn" disabled>
			Submit
		</Button>
		}
	</Form>
	</Fragment>
		)

}