import { useState, useEffect } from 'react';
import {Button, Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function CourseCard ({courseProp}) {


	//Checks to see if the data was successfully passed
	// console.log(props);
	// console.log(typeof props);

	//Deconstruct the course properties into their own variables(destructuring)
	const {_id, name, description, price} = courseProp;

	//Use the state hook for this component to be able to store its state
	//States are used to keep track of information related to individual component
	//Syntax
		//const [getter, setter] = useState(initialGetterValue)
		//getter = stored initial/default value
		//setter = updated value
	// const [count,setCount] = useState(0);
	// const [seat,setSeat] = useState(10);
	//state hook that indicates the button for enrollment
	// const [isOpen,setIsOpen] = useState(true);

	// useEffect(()=>{
	// 	if (seat === 0) {
	// 		setIsOpen(false);
	// 	}
	// }, [seat])

	// const enroll = () => {
	// 	if (seat<=0) {
	// 		alert('No more seats available');
	// 	}
	// 	else {
	// 		setCount(count + 1);
	// 		setSeat(seat - 1);
	// 	}
	// }

	return (
	<Card>
		<Card.Body>
			<Card.Title><h2>{name}</h2></Card.Title>	
			{/*<Card.Subtitle>Description</Card.Subtitle>
			<Card.Text>
				{description}
			</Card.Text>
			<Card.Subtitle>Price:</Card.Subtitle>
			<Card.Text>{price}</Card.Text>*/}
			{/*<Card.Subtitle>Enrollees</Card.Subtitle>
			<Card.Text>{count} Enrollees</Card.Text>
			<Card.Subtitle>Seats</Card.Subtitle>*/}
			{/*<Card.Text>{seat} Seats</Card.Text>*/}
			
			<Link className="btn btn-primary" to={`/courses/${_id}`}> Details </Link>

		</Card.Body>
	</Card>

	)
}


//Check if the courseCard Component is gettin the correct prop types
//Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from 1 component to the next
CourseCard.propTypes = {
	//The "shape" method is used to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

/*
mounting > updating > unmounting
mounting > rendering > re rendering > unmounting

login page (mounting) > typing data in inputs (rendering) (re rendering) > home page (unmounting)

*/