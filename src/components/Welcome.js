import {Fragment} from 'react';

export default function Welcome(props) {
	return (
		<Fragment>
			<h1>Hello, {props.name}. Age: {props.age}</h1>
			<p>Welcome to our Course Booking!</p>
		</Fragment>

		)
}